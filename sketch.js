//////////////////////////////// TWEETS ANALYSIS /////////////////////////////

//Run one tweet at a time by uncommenting it.
//Comment it again before running the next one.

//I had to connvert these from Processing to P5 so now some are longer than 140 chars :(

//Fredrik Oloffson's Tweets

 //--002
//i=0;function setup(){createCanvas(1200,900)}function draw(){for(j=0;j<99;)rect(i++%(1199-j++),int(i/99)%(999-j),i%12,j%16)}// #p5

//--0004
//s=900,i=j=0;function setup(){createCanvas(s,s);stroke(255,9);fill(9,3)}function draw(){quad(i++,j++,j,i,s-i,i-50,s-j,j);i=(i<<j%4)%1200;j=j%s}// #p5

//--0010
//i=0,k=450;function setup(){createCanvas(900,900);textSize(k)}function draw(){translate(k,k);fill(i%1*k/2,60);rotate(i+=+.01);text("$",99,0)}// #p5

//--0015
//i=0,s=900;function setup(){createCanvas(s,s);stroke(255,25)}function draw(){fill(i++%89,0,0,127);rect(i%90*9,i%91*9,i*i%92,i*i%93)}// #p5

//--0016
//i=0,s=900,t=1200;function setup(){createCanvas(t,s);noStroke()}function draw(){fill(i++%256,25);quad(i%t,i/3%s,i/4%t,i%s,i/5%t,i/4%s,i/3%t,i/2%s)}// #p5

//--0017
//t=0;function setup(){createCanvas(900,900);background(0);stroke(255,4)}function draw(){translate(450,450);line(sin(t)*421,cos(t++)*400,t%9,t%9)}// #p5




//--0022
//j=0;function setup(){createCanvas(1024,768)}function draw(){translate(512,384);i=frameCount;while(i-->1){rect(j++%i,j%i,3,i/9%9);rotate(0.009)}}// #p5

//j=0;
//function setup()
//{
//    createCanvas(1024,768)
//}
//function draw()
//{
//    //starting point of the drawing
//    translate(512,384);
//    //stroing the value of frame count in i 
//    i = frameCount;
//    //checking the value of i and if its more than 1 then making the rectangles
//    //since i is never going to be 0 as framecount can never be 0, the drawing keeps going on
//    while(i-- > 1)
//    {
//        rect ( j++ % i, j % i, 3, i/9 % 9);
//        //console.log(j);
//        //starting point-x, start-y, width, height
//        rotate(0.009);
//        //the speed at which the rotation takes place 
//    }
//}// #p5

/*This tweet by Fredrik Oloffson makes one think of a whirlpool. Once the entire canvas is covered by the rectangles and the next round of drawing begins, the user (if looked upon closely) can see a rotating flower. 
I found this particular tweet interesting due to the optical illusion it creates. The way the drawing keeps circling back and forth in the same direction, makes it seem like its going faster each time the next round begins. After some more time spent looking at it, it starts to look as if someone is shouting in the middle and then eventually their voice starts echoing. */

//t=0;function setup(){createCanvas(512,512)}function draw(){t+=0.001,i=0;translate(width/2,height/2);while(i<180){stroke(i%255,99,71);point(i++,i);rotate(t);}}

//t=0;
//function setup()
//{
//    createCanvas(512,512)
//}
//function draw()
//{
//    //variable that helps with the rotation of points and the cordinates of points
//    t += 0.001,i=0;
//    translate(width/2,height/2);
//    //Setting the start point as the middle of the screen
//    while(i<180)
//    {
//        stroke(i%255,90,71);
//        //Changing the color - starting from 0 and going all the way up to 255 and then starting from 0 again 
//        point(i++,i);
//        //Incrementing the value of i to make it a bigger circle
//        rotate(t);
//        //Rotating the point after executing it to give it a circular design 
//    }
//}

/*The following design was inspired by intricate Indian artowrk and Mandla art. The points get displayed slowly, hence making it easier for the viewer to follow the design as it keeps going. It's like a whirlpool with an eye in the middle. This artwork is similar to Indo-Islamic Architecture which can be seen in India. 
Eventually, the design starts resembling the giant sequoia in the Natural History Museum. The sequoia has a long history. The tree was 1,300 years old and 101 metres tall when it was felled. Its incredible lifespan is evident in its many rings and can be seen on a visit to the museum.*/


//Goldsmiths Students' Tweets

//Vytas Niedvaras 2015
//s=600;function setup(){createCanvas(s,s)}function draw(){i=frameCount;translate(i%s,0);stroke(i%255,7);for(i=0;i<800;){rotate(i++);line(0, i, i, 0)}}

//Lina Sarma 2015
//s=800,j=s/2;function setup(){createCanvas(s,s)}function draw(){translate(j,j);for(i=0;i<s;i++){rotate(2.95);rect(i*frameCount/30-10,i-10,i,i)}}

//Leon Fedden 2015
i=0;s=800;l=s/2;function setup(){createCanvas(s,s)}function draw(){j=sin(frameCount)*200;translate(l,l);rotate(i++%360);strokeWeight(0.1);line(j,j,0,0)}

//Jake Sparrowhunter 2016
//c=0,a=0,j=0;function setup(){createCanvas(384,384)}function draw(){a++;for(i=0;i<96;i++){for(j=0;j<96;j++){fill(c=i*j%a<<c<<i*j%a);rect(j*4,i*4,4,4)}}}
//this one is very advanced ... attempt at your peril

//Rebecca Johnson 2016
//s=0,r=250,t=500;function setup(){createCanvas(t,t);stroke(0,39)}function draw(){translate(r,r);line(cos(s+=0.0006)*50,sin(s++)*r,cos(s)*r,cos(s++))}

//Akhil Nair 2016
//w=0,s=900;function setup(){createCanvas(s,s)}function draw(){stroke(w*.32%255,w*.3%255,w*.28%255,50);line((w++>>s)%s,0,w++%s,s)}
